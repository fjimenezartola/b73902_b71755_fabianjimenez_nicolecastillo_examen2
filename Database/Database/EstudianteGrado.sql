﻿CREATE TABLE [dbo].[EstudianteGrado]
(
	[Carnet] NVARCHAR(10) NOT NULL PRIMARY KEY, 
    [Beca] BIT NULL, 
    CONSTRAINT [FK_EstudianteGrado_ToEstudiante] FOREIGN KEY ([Carnet]) REFERENCES [Estudiante]([Carnet])
)
