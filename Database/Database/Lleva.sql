﻿CREATE TABLE [dbo].[Lleva]
(
	[Carnet] NVARCHAR(10) NOT NULL,
	[NumeroGrupo] INT NOT NULL, 
    [SiglaCurso] NVARCHAR(50) NOT NULL, 
    [Periodo] NVARCHAR(50) NOT NULL
	Primary key (Carnet,NumeroGrupo,SiglaCurso,Periodo), 
    [Veces] INT NULL, 
    [Nota] INT NULL, 
    CONSTRAINT [FK_Lleva_ToGrupo] FOREIGN KEY ([NumeroGrupo],[SiglaCurso],[Periodo]) REFERENCES [Grupo]([NumeroGrupo],[SiglaCurso],[Periodo]), 
    CONSTRAINT [FK_Lleva_ToEstudiante] FOREIGN KEY ([Carnet]) REFERENCES [Estudiante]([Carnet])

)
