﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

MERGE INTO Curso AS Target
USING (VALUES
 ('IS', 5),
 ('BD', 5),
 ('PI', 3),
 ('DS', 5),
 ('INV1', 4),
 ('INV2', 4),
 ('INV3', 4),
 ('INV4', 4)
)
AS Source (Sigla, Creditos)
ON Target.Sigla = Source.Sigla
WHEN NOT MATCHED BY TARGET THEN
INSERT (Sigla, Creditos)
VALUES (Sigla, Creditos);

MERGE INTO Grupo AS Target
USING (VALUES
 (1,'IS', '2019-1'),
 (1,'BD', '2019-1'),
 (1,'PI', '2019-1'),
 (1,'DS', '2019-1'),
 (1,'INV1', '2019-1'),
 (1,'INV2', '2019-1'),
 (1,'INV3', '2019-1'),
 (1,'INV4', '2019-1')
)
AS Source (NumeroGrupo,SiglaCurso, Periodo)
ON Target.NumeroGrupo = Source.NumeroGrupo AND Target.SiglaCurso = Source.SiglaCurso AND Target.Periodo = Source.Periodo
WHEN NOT MATCHED BY TARGET THEN
INSERT (NumeroGrupo,SiglaCurso, Periodo)
VALUES (NumeroGrupo,SiglaCurso, Periodo);

MERGE INTO Estudiante AS Target
USING (VALUES
 ('A44148', 'Estudiante1','Grado',15000),
 ('A44149', 'Estudiante2','Grado',15000),
('A44150', 'Estudiante3','Posgrado',30000),
 ('A44151', 'Estudiante4','PosGrado',30000)
)
AS Source (Carnet, Nombre,Tipo,CostoCredito)
ON Target.Carnet = Source.Carnet
WHEN NOT MATCHED BY TARGET THEN
INSERT (Carnet, Nombre,Tipo,CostoCredito)
VALUES (Carnet, Nombre,Tipo,CostoCredito);

MERGE INTO EstudianteGrado AS Target
USING (VALUES
 ('A44148', 0),
 ('A44149', 0)
)
AS Source (Carnet, Beca)
ON Target.Carnet = Source.Carnet
WHEN NOT MATCHED BY TARGET THEN
INSERT (Carnet, Beca)
VALUES (Carnet, Beca);

MERGE INTO EstudiantePosgrado AS Target
USING (VALUES
 ('A44150', 1000000),
 ('A44151', 5000000)
)
AS Source (Carnet, Salario)
ON Target.Carnet = Source.Carnet
WHEN NOT MATCHED BY TARGET THEN
INSERT (Carnet, Salario)
VALUES (Carnet, Salario);

MERGE INTO Lleva AS Target
USING (VALUES
 ('A44148', 1,'IS','2019-1',1,100),
  ('A44148', 1,'BD','2019-1',1,90),
   ('A44148', 1,'PI','2019-1',1,80),
    ('A44148', 1,'DS','2019-1',1,70),
    
    ('A44149', 1,'IS','2019-1',2,100),
    ('A44149', 1,'BD','2019-1',2,90),
    ('A44149', 1,'PI','2019-1',2,80),
    ('A44149', 1,'DS','2019-1',2,70),

    ('A44150', 1,'INV1','2019-1',1,100),
    ('A44150', 1,'INV2','2019-1',1,90),
    ('A44150', 1,'INV3','2019-1',1,80),
    ('A44150', 1,'INV4','2019-1',1,70),

    ('A44151', 1,'INV1','2019-1',2,100),
    ('A44151', 1,'INV2','2019-1',2,90),
    ('A44151', 1,'INV3','2019-1',3,90),
    ('A44151', 1,'INV4','2019-1',2,70)

)
AS Source (Carnet, NumeroGrupo,SiglaCurso,Periodo,Veces,Nota)
ON Target.Carnet = Source.Carnet and Target.NumeroGrupo = Source.NumeroGrupo and Target.SiglaCurso = Source.SiglaCurso and Target.Periodo = Source.Periodo
WHEN NOT MATCHED BY TARGET THEN
INSERT (Carnet, NumeroGrupo,SiglaCurso,Periodo,Veces,Nota)
VALUES (Carnet, NumeroGrupo,SiglaCurso,Periodo,Veces,Nota);


