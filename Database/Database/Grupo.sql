﻿CREATE TABLE [dbo].[Grupo]
(
	[NumeroGrupo] INT NOT NULL, 
    [SiglaCurso] NVARCHAR(50) NOT NULL, 
    [Periodo] NVARCHAR(50) NOT NULL
    Constraint PK_Grupo
	Primary key (NumeroGrupo,SiglaCurso,Periodo), 
    CONSTRAINT [FK_Grupo_ToCurso] FOREIGN KEY ([SiglaCurso]) REFERENCES [Curso]([Sigla])
)
