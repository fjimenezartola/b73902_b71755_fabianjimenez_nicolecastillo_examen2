﻿CREATE TABLE [dbo].[Estudiante]
(
	[Carnet] NVARCHAR(10) NOT NULL PRIMARY KEY, 
    [Nombre] NVARCHAR(50) NULL, 
    [Tipo] NVARCHAR(50) NULL, 
    [CostoCredito] FLOAT NULL, 
)
