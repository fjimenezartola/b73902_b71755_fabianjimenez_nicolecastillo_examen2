﻿CREATE TABLE [dbo].[EstudiantePosgrado]
(
	[Carnet] NVARCHAR(10) NOT NULL PRIMARY KEY, 
    [Salario] FLOAT NULL
	CONSTRAINT [FK_EstudiantePosgrado_ToEstudiante] FOREIGN KEY ([Carnet]) REFERENCES [Estudiante]([Carnet])
)
