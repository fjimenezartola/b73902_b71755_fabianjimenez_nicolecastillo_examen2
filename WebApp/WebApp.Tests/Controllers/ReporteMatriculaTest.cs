﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebApp.Controllers.Reportes;
using WebApp.Controllers;
using System.Web.Mvc;
using WebApp.Models;
using Moq;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using WebApp.Servicios;

namespace WebApp.Tests.Controllers
{
    [TestClass]
    public class ReporteMatriculaTest
    {
        //Pruebas de unidad para calcular el costo de matricula
        [TestMethod]
        public void TestListaCalcularCostoMatriculaGrado()
        {
            ReporteMatriculaGrado reporte = new ReporteMatriculaGrado();
            List<String> cursos = new List<string>(){ "IS", "BD" };
            List<int> cantCred = new List<int>() {5,4};
            double costo = 1500;
            List< double > descuentosLista = new List<double>(){ 0.1,0.05};
            List<double> result = reporte.calcularCostoMatricula(cursos,cantCred,costo,descuentosLista);
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);
        }

        [TestMethod]
        public void TestResultadoCalcularCostoMatriculaGrado()
        {
            ReporteMatriculaGrado reporte = new ReporteMatriculaGrado();
            List<String> cursos = new List<string>() { "IS" };
            List<int> cantCred = new List<int>() {5};
            double costo = 1500;
            List<double> descuentosLista = new List<double>() { 0 };
            List<double> result = reporte.calcularCostoMatricula(cursos, cantCred, costo, descuentosLista);
            Assert.AreEqual(cantCred[0]* costo, result[0]);
        }

        [TestMethod]
        public void TestCalcularCostoMatriculaPosgrado()
        {
            ReporteMatriculaPosgrado reporte = new ReporteMatriculaPosgrado();
            List<String> cursos = new List<string>() { "IS", "BD" };
            List<int> cantCred = new List<int>() { 5, 4 };
            double costo = 1500;
            List<double> descuentosLista = new List<double>() { 0.1, 0.05 };
            List<double> result = reporte.calcularCostoMatricula(cursos, cantCred, costo, descuentosLista);
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);
        }

        [TestMethod]
        public void TestResultadoCalcularCostoMatriculaPosGrado()
        {
            ReporteMatriculaPosgrado reporte = new ReporteMatriculaPosgrado();
            List<String> cursos = new List<string>() { "IS" };
            List<int> cantCred = new List<int>() { 5 };
            double costo = 1500;
            List<double> descuentosLista = new List<double>() { 0 };
            List<double> result = reporte.calcularCostoMatricula(cursos, cantCred, costo, descuentosLista);
            Assert.AreEqual(cantCred[0] * costo, result[0]);
        }

        [TestMethod]
        public void TestconsultaReporteEstadoCuentaMock()
        {
            ReporteMatriculaGrado reporte = new ReporteMatriculaGrado();

            var estudiantes = new List<EstudianteGrado>
             {
             new EstudianteGrado() { Carnet = "B1", Nombre = "Javier" },
             new EstudianteGrado() { Carnet = "BB", Nombre = "Aneki" },
             }.AsQueryable();

            var cursos = new List<Curso> {
                new Curso(){Sigla = "IS"},
            }.AsQueryable();

            var grupo = new List<Grupo> {
                new Grupo(){NumeroGrupo=1,Periodo = "2019-1", SiglaCurso = "IS"}
            }.AsQueryable();

            var lleva = new List<Lleva> {
                new Lleva(){Carnet = "B1",NumeroGrupo=1,Periodo = "2019-1", SiglaCurso = "IS"}
            }.AsQueryable();

            var mockDbSet = new Mock<DbSet<EstudianteGrado>>();
            mockDbSet.As<IQueryable<EstudianteGrado>>().Setup(m => m.Provider).Returns(estudiantes.Provider);
            mockDbSet.As<IQueryable<EstudianteGrado>>().Setup(m => m.Expression).Returns(estudiantes.Expression);
            mockDbSet.As<IQueryable<EstudianteGrado>>().Setup(m => m.ElementType).Returns(estudiantes.ElementType);
            mockDbSet.As<IQueryable<EstudianteGrado>>().Setup(m => m.GetEnumerator()).Returns(estudiantes.GetEnumerator());

            var mockDbSet1 = new Mock<DbSet<Estudiante>>();
            mockDbSet1.As<IQueryable<Estudiante>>().Setup(m => m.Provider).Returns(estudiantes.Provider);
            mockDbSet1.As<IQueryable<Estudiante>>().Setup(m => m.Expression).Returns(estudiantes.Expression);
            mockDbSet1.As<IQueryable<Estudiante>>().Setup(m => m.ElementType).Returns(estudiantes.ElementType);
            mockDbSet1.As<IQueryable<Estudiante>>().Setup(m => m.GetEnumerator()).Returns(estudiantes.GetEnumerator());

            var mockDbSet2 = new Mock<DbSet<Lleva>>();
            mockDbSet2.As<IQueryable<Lleva>>().Setup(m => m.Provider).Returns(lleva.Provider);
            mockDbSet2.As<IQueryable<Lleva>>().Setup(m => m.Expression).Returns(lleva.Expression);
            mockDbSet2.As<IQueryable<Lleva>>().Setup(m => m.ElementType).Returns(lleva.ElementType);
            mockDbSet2.As<IQueryable<Lleva>>().Setup(m => m.GetEnumerator()).Returns(lleva.GetEnumerator());

            var mockDbSet3 = new Mock<DbSet<Grupo>>();
            mockDbSet3.As<IQueryable<Grupo>>().Setup(m => m.Provider).Returns(grupo.Provider);
            mockDbSet3.As<IQueryable<Grupo>>().Setup(m => m.Expression).Returns(grupo.Expression);
            mockDbSet3.As<IQueryable<Grupo>>().Setup(m => m.ElementType).Returns(grupo.ElementType);
            mockDbSet3.As<IQueryable<Grupo>>().Setup(m => m.GetEnumerator()).Returns(grupo.GetEnumerator());


            var mockDbSet4 = new Mock<DbSet<Curso>>();
            mockDbSet4.As<IQueryable<Curso>>().Setup(m => m.Provider).Returns(cursos.Provider);
            mockDbSet4.As<IQueryable<Curso>>().Setup(m => m.Expression).Returns(cursos.Expression);
            mockDbSet4.As<IQueryable<Curso>>().Setup(m => m.ElementType).Returns(cursos.ElementType);
            mockDbSet4.As<IQueryable<Curso>>().Setup(m => m.GetEnumerator()).Returns(cursos.GetEnumerator());

           

            var mockDb = new Mock<ModeloContext>();
            mockDb.Setup(m => m.Estudiantes).Returns(mockDbSet1.Object);
            mockDb.Setup(m => m.EstudiantesGrado).Returns(mockDbSet.Object);
            mockDb.Setup(m => m.Llevan).Returns(mockDbSet2.Object);
            mockDb.Setup(m => m.Grupos).Returns(mockDbSet3.Object);
            mockDb.Setup(m => m.Cursos).Returns(mockDbSet4.Object);

            mockDb.Setup(m => m.EstudiantesGrado.Find("B1")).Returns(estudiantes.ToList()[0]);
            mockDb.Setup(m => m.EstudiantesGrado.Find("BB")).Returns(estudiantes.ToList()[1]);
            mockDb.Setup(m => m.Estudiantes.Find("B1")).Returns(estudiantes.ToList()[0]);
            mockDb.Setup(m => m.Estudiantes.Find("BB")).Returns(estudiantes.ToList()[1]);
            mockDb.Setup(m => m.Llevan.Find("B1",1,"2019-1","IS")).Returns(lleva.ToList()[0]);

            ServicioConsultasParaReporte serv = new ServicioConsultasParaReporte();
            List<InfoReporte> result = reporte.consultaReporteEstadoCuenta("B1", mockDb.Object,serv); 
            Assert.IsNotNull(result);
        }


    }
}
