﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebApp.Controllers.Reportes;
using WebApp.Controllers;
using System.Web.Mvc;
using WebApp.Models;
using Moq;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;


namespace WebApp.Tests.Controllers
{
    [TestClass]
    public class EstudianteControllerTest
    {
        [TestMethod]
        public void TestgenerarReporteEstadoCuenta()
        {

            EstudianteController controller = new EstudianteController();
            ViewResult result = controller.generarReporteEstadoCuenta("A44148") as ViewResult;
            Assert.IsNotNull(result);

        }

        [TestMethod]
        public void TestgenerarReporteEstadoCuenta2()
        {

            EstudianteController controller = new EstudianteController();
            ViewResult result = controller.generarReporteEstadoCuenta("BB") as ViewResult;
            Assert.IsNotNull(result);

        }

        [TestMethod]
        public void TestgenerarReporteCobrosMatricula()
        {

            EstudianteController controller = new EstudianteController();
            ViewResult result = controller.generarReporteCobrosMatricula() as ViewResult;
            Assert.IsNotNull(result);

        }
    }
}
