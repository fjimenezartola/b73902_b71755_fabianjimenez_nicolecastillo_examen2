﻿using System;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Interactions;
using WebApp;
using WebApp.Controllers;
using System.Web.Mvc;
using WebApp.Models;
using System.Collections.Generic;

namespace SeleniumTests
{
    [TestClass]
    public class Selenium
    {
        IWebDriver driver;
        [TestCleanup]
        public void TearDown()
        {
            if (driver != null)
                driver.Quit();
        }


        [TestMethod]
        public void PruebaAdministrarEstudianteGrado()
        {
            ///Arrange
            /// Crea el driver de Chrome
            EstudianteGradoController controller = new EstudianteGradoController();
            driver = new ChromeDriver();
            CreateTest(controller);
            DeleteTest(controller);
            //EditTest();
        }

        /*private void PruebaIngreso()
        {
            ///Arrange
            /// Pone la pantalla en full screen
            driver.Manage().Window.Maximize();
            ///Act
            /// Se va a la URL de la aplicacion
            driver.Url = "https://localhost:44301/";
            ///Assert
            Assert.AreEqual(driver.FindElement(By.XPath("//h2")).Text, "Productos más recientes");
        }*/
        [TestMethod]
        private void CreateTest(EstudianteGradoController controller)
        {
            driver.Manage().Window.Maximize();
            driver.Url = "https://localhost:44301/EstudianteGrado";
            ViewResult result1 = controller.Index() as ViewResult;
            List<EstudianteGrado>estudiante1 = (List<EstudianteGrado>)result1.ViewData.Model;
            int size1 = estudiante1.Count;
            driver.FindElement(By.Id("create")).Click();
            driver.FindElement(By.Id("editorCarnet")).SendKeys("B99");
            driver.FindElement(By.Id("editorNombre")).SendKeys("NombrePrueba");
            driver.FindElement(By.Id("editorCostoCredito")).SendKeys("1900");
            driver.FindElement(By.Id("editorBeca")).Click();
            driver.FindElement(By.Id("save")).Click();
            ViewResult result2 = controller.Index() as ViewResult;
            List<EstudianteGrado> estudiante2 = (List<EstudianteGrado>)result2.ViewData.Model;
            Assert.AreEqual(size1 + 1, estudiante2.Count);
        }

        [TestMethod]
        private void DeleteTest(EstudianteGradoController controller)
        {
            driver.Manage().Window.Maximize();
            driver.Url = "https://localhost:44301/EstudianteGrado/Delete/B99";
            ViewResult result1 = controller.Index() as ViewResult;
            List<EstudianteGrado> estudiante1 = (List<EstudianteGrado>)result1.ViewData.Model;
            int size1 = estudiante1.Count;
            driver.FindElement(By.Id("delete")).Click();
            ViewResult result2 = controller.Index() as ViewResult;
            List<EstudianteGrado> estudiante2 = (List<EstudianteGrado>)result2.ViewData.Model;
            Assert.AreEqual(size1 - 1, estudiante2.Count);
        }
    }
}
