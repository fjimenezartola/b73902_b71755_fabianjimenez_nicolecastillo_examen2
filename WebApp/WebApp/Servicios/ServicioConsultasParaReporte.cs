﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApp.Models;

namespace WebApp.Servicios
{
    public class ServicioConsultasParaReporte
    {
        public String consultaNombre(String carnet, ModeloContext db)
        {
            var consulta = (from e in db.Estudiantes
                            where e.Carnet == carnet
                            select e.Nombre).SingleOrDefault();


            return consulta;
        }

        public int consultaNumGrupo(String Sigla, String carnet, ModeloContext db)
        {
            var consulta = (from l in db.Llevan
                            where l.Carnet == carnet && l.SiglaCurso == Sigla
                            select l.NumeroGrupo).SingleOrDefault();


            return consulta;

        }
        public int consultaVeces(String Sigla, String carnet, ModeloContext db)
        {
            var consulta = (from l in db.Llevan
                            where l.Carnet == carnet && l.SiglaCurso == Sigla
                            select l.Veces).SingleOrDefault();


            return consulta;

        }
        public String consultaPeriodo(String Sigla, String carnet, ModeloContext db)
        {
            var consulta = (from l in db.Llevan
                            where l.Carnet == carnet && l.SiglaCurso == Sigla
                            select l.Periodo).SingleOrDefault();


            return consulta;

        }
        public int consultaNota(String carnet, String Sigla, ModeloContext db)
        {
            var consulta = (from l in db.Llevan
                            join g in db.Grupos on new { l.SiglaCurso, l.Periodo, l.NumeroGrupo } equals new { g.SiglaCurso, g.Periodo, g.NumeroGrupo }
                            join c in db.Cursos on l.SiglaCurso equals c.Sigla
                            where l.Carnet == carnet && l.SiglaCurso == Sigla
                            select l.Nota).SingleOrDefault();

            return consulta;

        }
        public List<String> consultaCursos(String carnet, ModeloContext db)
        {
            var consulta = (from l in db.Llevan
                            where l.Carnet == carnet
                            select l.SiglaCurso
                            ).ToList();
            return consulta;

        }

        public int consultaCreditos(String Sigla, ModeloContext db)
        {
            var consulta = (from c in db.Cursos
                            where c.Sigla == Sigla
                            select c.Creditos
                           ).SingleOrDefault();
            return consulta;

        }

        public double consultaCostoCreditos(String carnet, ModeloContext db)
        {
            var consulta = (from e in db.Estudiantes
                            where e.Carnet == carnet
                            select e.CostoCredito
                           ).SingleOrDefault();
            return consulta;

        }

        public int consultaCantidadCreditos(List<String> siglas ,ModeloContext db)
        {
            int suma = 0;
            foreach(var i in siglas)
            {
                suma += consultaCreditos(i, db);
            }
            return suma;
        }
    }
}