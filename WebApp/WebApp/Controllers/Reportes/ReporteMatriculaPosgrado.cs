﻿using System;
using System.Activities.Expressions;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Razor.Generator;
using WebApp.Models;
using WebApp.Servicios;

namespace WebApp.Controllers.Reportes
{
    public class ReporteMatriculaPosgrado : IReporteMatricula
    {

        public List<double> calcularCostoMatricula(List<String> cursos, List<int> cantCred, double costo, List<double> descuentosLista)
        {
            List<double> totalPorCurso = new List<double>();
            double costoCurso = 0;
            for (int i = 0; i < cursos.Count(); ++i)
            {

                costoCurso = costo * cantCred[i];
                if(descuentosLista[i] == 0)
                {
                    totalPorCurso.Add(costoCurso);
                }
                else
                {
                    costoCurso -= costoCurso * descuentosLista[i];
                    totalPorCurso.Add(costoCurso);
                }

                costoCurso = 0;
            }


            return totalPorCurso;
        }
        public List<InfoReporte> consultaReporteCobrosMatricula(ModeloContext db, ServicioConsultasParaReporte serv)
        {
            var consultaCarnet = (from eg in db.EstudiantesPosgrado
                                  select eg.Carnet).ToList();
            List<InfoReporte> listaEstudiantesPosgrado = new List<InfoReporte>();
            for (int i = 0; i < consultaCarnet.Count(); ++i)
            {

                listaEstudiantesPosgrado = listaEstudiantesPosgrado.Union(consultaReporteEstadoCuenta(consultaCarnet[i], db,serv)).ToList();
            }
            return listaEstudiantesPosgrado;
        }
        private List<double> calcularDescuento(List<String> cursos, List<int> notas,  double salario)
        {
            List<double> descuentoLista = new List<double>();
            double descuento = 0;
            for (int i = 0; i < cursos.Count(); ++i)
            {
                
                if (notas[i] >= 95)
                {
                    descuento = 0.10;
                }
                else if (notas[i] >= 90)
                {
                    descuento = 0.05;
                }

                if(salario >= 5000000)
                {
                    descuento = 0;
                }
                descuentoLista.Add(descuento);
                descuento = 0;
            }

            return descuentoLista;

        }

        public List<InfoReporte> consultaReporteEstadoCuenta(String carnet, ModeloContext db, ServicioConsultasParaReporte serv)
        {
            List<String> cursos = serv.consultaCursos(carnet, db);
            List<int> notas = new List<int>();
            List<int> cantCreditos = new List<int>();
            foreach (var c in cursos)
            {
                notas.Add(serv.consultaNota(carnet, c, db));//lista de notas en el mismo orden que la lista de cursos
                cantCreditos.Add(serv.consultaCreditos(c, db));
            }
            double salario = consultaSalario(carnet, db);
            
            double costo = serv.consultaCostoCreditos(carnet, db);
            List<double> descuentos = calcularDescuento(cursos, notas, salario);
            List<double> costosTotal = calcularCostoMatricula(cursos, cantCreditos, costo, descuentos);
            List<InfoReporte> reporteEstadoCuenta = llenarLista(carnet, cursos, notas, cantCreditos, costo, descuentos, db, costosTotal, serv);

            return reporteEstadoCuenta;
        }

        private List<InfoReporte> llenarLista(String carnet, List<String> cursos, List<int> notas, List<int> cantCred, double costo, List<double> descuentosLista, ModeloContext db, List<double> costoTotal, ServicioConsultasParaReporte serv)
        {
            List<InfoReporte> reporte = new List<InfoReporte>();
            for (int i = 0; i < cursos.Count(); ++i)
            {
                String nombre = serv.consultaNombre(carnet, db);
                if (serv.consultaCantidadCreditos(cursos, db) > 25)
                {
                    nombre += nombre + "(*)";
                }
                reporte.Add(new InfoReporte(carnet, nombre, "Posgrado", serv.consultaPeriodo(cursos[i], carnet, db), cursos[i], serv.consultaNumGrupo(cursos[i], carnet, db), serv.consultaVeces(cursos[i], carnet, db), notas[i], cantCred[i], descuentosLista[i], costo, costoTotal[i]));
            }
            return reporte;
        }

        private double consultaSalario(String carnet,ModeloContext db)
        {
            var consulta = (from ep in db.EstudiantesPosgrado
                            where ep.Carnet == carnet
                            select ep.Salario).SingleOrDefault();
            return consulta;
        }

    }
}