﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebApp.Models;
using WebApp.Servicios;
namespace WebApp.Controllers.Reportes
{
    interface IReporteMatricula
    {
        List<InfoReporte> consultaReporteEstadoCuenta(String carnet, ModeloContext db, ServicioConsultasParaReporte serv);
        List<InfoReporte> consultaReporteCobrosMatricula(ModeloContext db, ServicioConsultasParaReporte serv);
        List<double> calcularCostoMatricula(List<String> cursos, List<int> cantCred, double costo, List<double> descuentosLista);
    }
}