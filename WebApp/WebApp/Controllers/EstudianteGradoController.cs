﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class EstudianteGradoController : Controller
    {
        private ModeloContext db; // 

        public EstudianteGradoController(ModeloContext db)
        {
            this.db = db;

        }

        public EstudianteGradoController()
        {
            this.db = new ModeloContext();

        }
        // GET: EstudianteGrado
        public ActionResult Index()
        {
            return View("Index",db.EstudiantesGrado.ToList());
        }

        public ActionResult Create()
        {
            ViewBag.Carnet = new SelectList(db.Estudiantes, "Carnet", "Nombre");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Carnet,Nombre,CostoCredito,Beca")] EstudianteGrado estudianteGrado)
        {
            if (ModelState.IsValid)
            {
                estudianteGrado.Tipo = "Grado";
                db.EstudiantesGrado.Add(estudianteGrado);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Cedula = new SelectList(db.Estudiantes, "Cedula", "Nombre", estudianteGrado.Carnet);
            return View(estudianteGrado);
        }

        public ActionResult Edit(String id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstudianteGrado estudianteGrado = db.EstudiantesGrado.Find(id);
            if (estudianteGrado == null)
            {
                return HttpNotFound();
            }
            ViewBag.Cedula = new SelectList(db.Estudiantes, "Carnet", "Nombre", estudianteGrado.Carnet);
            return View("Edit", estudianteGrado);
        }

        [HttpPost]
        [ValidateAntiForgeryToken] //dice cual es el handler del form , en la vista tambien se pone que se usa un handler "AntiForgeryToken"
        public ActionResult Edit([Bind(Include = "Carnet,Nombre,CostoCredito,Tipo,Beca")] EstudianteGrado estudianteGrado)
        {
            if (ModelState.IsValid)
            {
                db.Entry(estudianteGrado).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Cedula = new SelectList(db.Estudiantes, "Carnet", "Nombre", estudianteGrado.Carnet);
            return View(estudianteGrado);
        }
        public ActionResult Details(String id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstudianteGrado estudiante = db.EstudiantesGrado.Find(id);
            if (estudiante == null)
            {
                return HttpNotFound();
            }
            return View(estudiante);
        }

        public ActionResult Delete(String id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstudianteGrado estudiante = db.EstudiantesGrado.Find(id);
            if (estudiante == null)
            {
                return HttpNotFound();
            }
            return View(estudiante);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(String id)
        {
            EstudianteGrado estudiante = db.EstudiantesGrado.Find(id);
            db.EstudiantesGrado.Remove(estudiante);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

       

    }
}