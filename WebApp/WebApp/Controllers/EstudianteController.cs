﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApp.Controllers.Reportes;
using WebApp.Models;
using WebApp.Servicios;
namespace WebApp.Controllers
{
    public class EstudianteController : Controller
    {
        private ModeloContext db; // 
        ServicioConsultasParaReporte serv = new ServicioConsultasParaReporte();
        public EstudianteController(ModeloContext db)
        {
            this.db = db;

        }

        public EstudianteController()
        {
            this.db = new ModeloContext();

        }
        // GET: Estudiante
        public ActionResult Index()
        {
            return View("Index",db.Estudiantes.ToList());
        }

        public ActionResult generarReporteEstadoCuenta(String Carnet)
        {
            IReporteMatricula IR;
            String tipo = consultaTipo(Carnet);
            if (tipo == "Grado")
            {
                IR = new ReporteMatriculaGrado();
            }
            else
            {
                IR = new ReporteMatriculaPosgrado();
            }

            List<InfoReporte> lista = IR.consultaReporteEstadoCuenta(Carnet, db,serv);
            ViewBag.lista = lista;
            return View("EstudianteReporte", lista);
        }
        public ActionResult generarReporteCobrosMatricula()
        {
            IReporteMatricula IRGrado = new ReporteMatriculaGrado();
            IReporteMatricula IRPosgrado = new ReporteMatriculaPosgrado();
            List<InfoReporte> lista = (IRGrado.consultaReporteCobrosMatricula(db,serv)).Union(IRPosgrado.consultaReporteCobrosMatricula(db,serv)).ToList();
            ViewBag.lista = lista;
            return View("_Reporte", lista);
        }
        private String consultaTipo(String carnet) {
            var consulta = (from e in db.Estudiantes
                            where e.Carnet == carnet
                            select e.Tipo).SingleOrDefault();

            return consulta;
        }
    }
}