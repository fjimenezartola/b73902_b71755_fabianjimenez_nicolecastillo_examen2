﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.Models
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("EstudianteGrado")]
    public class EstudianteGrado : Estudiante
    {
        public bool Beca { get; set; }
    }
}