﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.IO;
using System.Drawing;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models
{
    public class InfoReporte
    {
        public String Carnet { get; set; }
        public String Nombre { get; set; }
        public String Tipo { get; set; }
        public String Periodo { get; set; }
        public String SiglaCurso { get; set; }
        public int NumeroGrupo { get; set; }
        public int Veces { get; set; }
        public int Nota { get; set; }
        public int Creditos { get; set; }
        public double Descuento { get; set; }
        public double CostoCredito { get; set; }
        public double MontoTotal { get; set; }
        public InfoReporte(String Carnet, String Nombre,String Tipo,String Periodo,String SiglaCurso,int NumeroGrupo, int Veces,int Nota,int Creditos, double Descuento, double costoCredito,double MontoTotal)
        {
            this.Carnet = Carnet;
            this.Nombre = Nombre;
            this.Tipo = Tipo;
            this.Periodo = Periodo;
            this.SiglaCurso = SiglaCurso;
            this.NumeroGrupo = NumeroGrupo;
            this.Veces = Veces;
            this.Nota = Nota;
            this.Creditos = Creditos;
            this.Descuento = Descuento;
            this.CostoCredito = costoCredito;
            this.MontoTotal = MontoTotal;

        }
    }
}