﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.Models
{
    //using System;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.ComponentModel.DataAnnotations;


    [Table("Lleva")]
    public class Lleva
    {
        [Key, Column(Order = 1)]
        public String Carnet { get; set; }
        [Key, Column(Order = 2)]
        public int NumeroGrupo { get; set; }
        [Key, Column(Order = 3)]
        public String SiglaCurso { get; set; }
        [Key, Column(Order = 4)]
        public String Periodo { get; set; }
        public int Veces { get; set; }
        public int Nota { get; set; }
        //public int Descuento { get; set; }
    }
}