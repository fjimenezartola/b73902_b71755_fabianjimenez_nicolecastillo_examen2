﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.Models
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("EstudiantePosgrado")]
    public class EstudiantePosgrado:Estudiante
    {
        public double Salario { get; set; }
    }
}