﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Estudiante")] 
    public abstract class Estudiante
    {
        [Key]
        public String Carnet { get; set; }
        public String Nombre { get; set; }
        public double CostoCredito { get; set; }
        public String Tipo { get; set; }
    }
}