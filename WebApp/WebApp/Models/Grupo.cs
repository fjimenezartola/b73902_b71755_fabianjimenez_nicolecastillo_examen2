﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.Models
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.ComponentModel.DataAnnotations;

    [Table("Grupo")]
    public class Grupo
    {
        [Key, Column(Order = 1)]
        public int NumeroGrupo { get; set; }
        [Key, Column(Order = 2)]
        public String Periodo { get; set; }
        [Key, Column(Order = 3)]
        public String SiglaCurso { get; set; }
       
    }
}