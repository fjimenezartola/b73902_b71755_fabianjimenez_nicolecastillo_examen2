﻿namespace WebApp.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;

    public class ModeloContext : DbContext
    {
        public ModeloContext()
            : base("name=Database")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Estudiante>().ToTable("Estudiante");
            modelBuilder.Entity<EstudianteGrado>().ToTable("EstudianteGrado");
            modelBuilder.Entity<EstudiantePosgrado>().ToTable("EstudiantePosgrado");
            modelBuilder.Entity<Lleva>().ToTable("Lleva");
            modelBuilder.Entity<Curso>().ToTable("Curso");
            modelBuilder.Entity<Grupo>().ToTable("Grupo");
        }

        public virtual DbSet<Estudiante> Estudiantes { get; set; }
        public virtual DbSet<EstudianteGrado> EstudiantesGrado { get; set; }
        public virtual DbSet<EstudiantePosgrado> EstudiantesPosgrado { get; set; }
        public virtual DbSet<Lleva> Llevan { get; set; }
        public virtual DbSet<Curso> Cursos { get; set; }
        public virtual DbSet<Grupo> Grupos { get; set; }
    }
}