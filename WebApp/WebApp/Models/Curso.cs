﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Curso")]
    public class Curso
    {
        [Key]
        public String Sigla { get; set; }
        public int Creditos { get; set; }

    }
}